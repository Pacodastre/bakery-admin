import React from "react";
import { connect } from "react-redux";
import { Table } from "semantic-ui-react";

export const TrolleySmall = ({
  productManager,
  trolley,
  withAllergens = false,
  allergenManager,
  specialOfferManager,
}) => (
  <Table unstackable>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Product</Table.HeaderCell>
        <Table.HeaderCell textAlign="right">Quantity</Table.HeaderCell>
        <Table.HeaderCell textAlign="right">Unit Price</Table.HeaderCell>
        <Table.HeaderCell textAlign="right">Price</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {trolley.items.map((trolleyItem) => {
        const product =
          trolleyItem.product_id &&
          productManager.getProduct(trolleyItem.product_id);
        const specialOffer =
          trolleyItem.special_offer_id &&
          specialOfferManager.getSpecialOffer(trolleyItem.special_offer_id);
        return (
          <Table.Row key={trolleyItem.id}>
            <Table.Cell>
              {trolleyItem.isProduct() ? product.name : specialOffer.name}
              {withAllergens && product.allergens.length > 0 && (
                <>
                  <br />
                  Contains:{" "}
                  {product.allergens.map(
                    (allergen) => allergenManager.getAllergen(allergen).name
                  )}
                </>
              )}
            </Table.Cell>
            <Table.Cell textAlign="right">{trolleyItem.quantity}</Table.Cell>
            <Table.Cell textAlign="right">
              {trolleyItem.unit_price.toFixed(2)}
            </Table.Cell>
            <Table.Cell textAlign="right">
              {(
                Math.round(
                  trolleyItem.quantity * trolleyItem.unit_price * 100
                ) / 100
              ).toFixed(2)}
            </Table.Cell>
          </Table.Row>
        );
      })}
      <Table.Row>
        <Table.Cell>Delivery</Table.Cell>
        <Table.Cell></Table.Cell>
        <Table.Cell></Table.Cell>
        <Table.Cell textAlign="right">
          {trolley.delivery_price.toFixed(2)}
        </Table.Cell>
      </Table.Row>
    </Table.Body>
    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell>Total</Table.HeaderCell>
        <Table.HeaderCell></Table.HeaderCell>
        <Table.HeaderCell></Table.HeaderCell>
        <Table.HeaderCell textAlign="right">
          €{trolley.calculateTotal().toFixed(2)}
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  </Table>
);

const mapStateToProps = ({ order, productManager, allergenManager }) => ({
  trolley: order.trolley,
  productManager,
  allergenManager,
});

export default connect(mapStateToProps)(TrolleySmall);
