import React from "react";
import { Header } from "semantic-ui-react";

const CustomerReceipt = ({ customer, delivery, isDelivery, payment }) => (
  <>
    <Header as="h3">
      <Header.Content>Delivery Information</Header.Content>
      <Header sub>
        Delivery on: {delivery.formatField("delivery_date")}
        <br />
        Between {`${delivery.time_slot}`.padStart(2, "0")}:00 and{" "}
        {`${delivery.time_slot + 1}`.padStart(2, "0")}
        :00
      </Header>
    </Header>
    {customer.first_name} {customer.last_name}
    <br />
    Payment by: {payment.type}
    <br />
    Phone: {customer.phone}
    <br />
    {isDelivery && (
      <>
        Address: {customer.address1}
        <br />
        {customer.address2}
        <br />
        {customer.city}
        <br />
        {customer.county}
      </>
    )}
  </>
);

export default CustomerReceipt;
