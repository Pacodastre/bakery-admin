import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Form } from "semantic-ui-react";
import { Redirect, useHistory } from "react-router-dom";

import Session from "domain-objects/session";
import { login, isUserLoggedIn } from "redux/actions/session";

const Login = ({ login }) => {
  const [session, setSession] = useState(new Session());
  const history = useHistory();
  if (isUserLoggedIn()) {
    return <Redirect to="/" />;
  }
  return (
    <Form
      onSubmit={async (event) => {
        event.preventDefault();
        await login(session);
        history.push("/orders/confirmed");
      }}
    >
      <Form.Field>
        <label htmlFor="email">Email</label>
        <input
          placeholder="Email"
          name="email"
          id="email"
          value={session.email}
          onChange={(event) =>
            setSession(session.update(event.target.name, event.target.value))
          }
        />
      </Form.Field>
      <Form.Field>
        <label htmlFor="password">Password</label>
        <input
          placeholder="Password"
          name="password"
          id="password"
          type="password"
          value={session.password}
          onChange={(event) =>
            setSession(session.update(event.target.name, event.target.value))
          }
        />
      </Form.Field>
      <Button type="submit">Login</Button>
    </Form>
  );
};

const mapDispatchToProps = { login };

export default connect(null, mapDispatchToProps)(Login);
