import React from "react";
import { connect } from "react-redux";
import { Header } from "semantic-ui-react";

export const AllergenInfo = ({
  trolley,
  productManager,
  allergenManager,
  specialOfferManager,
}) => (
  <div>
    <Header as="h3">
      <Header.Content>Allergen Information</Header.Content>
    </Header>
    {trolley.items.map((trolleyItem) => {
      const product =
        trolleyItem.product_id && trolleyItem.getProduct(productManager);
      const specialOffer =
        trolleyItem.special_offer_id &&
        specialOfferManager.getSpecialOffer(trolleyItem.special_offer_id);
      const allergens = trolleyItem.isProduct()
        ? product.getAllergens(allergenManager)
        : specialOffer
            .getProducts(productManager)
            .getAllergens(allergenManager);
      const mayContainAllergens = trolleyItem.isProduct()
        ? product.getMayContainAllergens(allergenManager)
        : specialOffer
            .getProducts(productManager)
            .getMayContainAllergens(allergenManager);
      return (
        <span>
          <strong>
            {trolleyItem.isProduct() ? product.name : specialOffer.name}
          </strong>
          <br />
          Contains:{" "}
          {allergens.items.map((allergen) => allergen.name).join(", ")}
          <br />
          {mayContainAllergens.items.length > 0 && (
            <>
              May contain:{" "}
              {mayContainAllergens.items
                .map((allergen) => allergen.name)
                .join(", ")}
              <br />
            </>
          )}
        </span>
      );
    })}
  </div>
);

const mapStateToProps = ({
  order,
  productManager,
  allergenManager,
  specialOfferManager,
}) => ({
  trolley: order.trolley,
  productManager,
  allergenManager,
  specialOfferManager,
});

export default connect(mapStateToProps)(AllergenInfo);
