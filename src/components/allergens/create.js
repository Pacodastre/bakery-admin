import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import Allergen from "domain-objects/allergen";
import { createAllergen } from "redux/actions/allergen";
import AllergenForm from "components/allergens/form";

const CreateAllergen = ({ createAllergen = () => {} }) => {
  const [stateAllergen, setAllergen] = useState(new Allergen());
  const history = useHistory();
  return (
    <AllergenForm
      onSubmit={async (event) => {
        event.preventDefault();
        await createAllergen(stateAllergen);
        history.push("/settings/allergens");
      }}
      allergen={stateAllergen}
      setAllergen={setAllergen}
      submitButtonText="Create Allergen"
    />
  );
};

const mapDispatchToProps = { createAllergen };

export default connect(null, mapDispatchToProps)(CreateAllergen);
