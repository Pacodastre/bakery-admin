import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import Allergen from "domain-objects/allergen";
import { updateAllergen } from "redux/actions/allergen";
import AllergenForm from "components/allergens/form";

const EditAllergen = ({ updateAllergen, allergen }) => {
  const [stateAllergen, setAllergen] = useState(new Allergen());
  const history = useHistory();
  useEffect(() => {
    setAllergen(allergen);
  }, [setAllergen, allergen]);
  return (
    <AllergenForm
      onSubmit={async (event) => {
        event.preventDefault();
        await updateAllergen(stateAllergen);
        history.push("/settings/allergens");
      }}
      allergen={stateAllergen}
      setAllergen={setAllergen}
      submitButtonText="Update Allergen"
    />
  );
};

const mapStateToProps = ({ allergen }) => ({ allergen });

const mapDispatchToProps = { updateAllergen };

export default connect(mapStateToProps, mapDispatchToProps)(EditAllergen);
