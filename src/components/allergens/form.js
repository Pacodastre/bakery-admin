import React from "react";
import { Button, Form } from "semantic-ui-react";

export default ({ onSubmit, allergen, setAllergen, submitButtonText }) => (
  <Form onSubmit={onSubmit}>
    <Form.Field>
      <label htmlFor="name">Name</label>
      <input
        type="text"
        name="name"
        onChange={(event) =>
          setAllergen(allergen.update(event.target.name, event.target.value))
        }
        value={allergen.name || ""}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="name">Image</label>
      <input
        type="text"
        name="image"
        onChange={(event) =>
          setAllergen(allergen.update(event.target.name, event.target.value))
        }
        value={allergen.image || ""}
      />
    </Form.Field>
    <Button type="submit">{submitButtonText}</Button>
  </Form>
);
