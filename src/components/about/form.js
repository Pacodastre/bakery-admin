import React from "react";
import { Button, Form, TextArea } from "semantic-ui-react";

export default ({
  onSubmit,
  settingManager,
  setSettingManager,
  submitButtonText,
}) => (
  <Form onSubmit={onSubmit}>
    <Form.Field>
      <label htmlFor="about">About</label>
      <TextArea
        type="text"
        name="about"
        onChange={(event) =>
          setSettingManager(
            settingManager.updateSetting(event.target.name, event.target.value)
          )
        }
        value={settingManager.getByName("about").value || ""}
      />
    </Form.Field>
    <Button type="submit">{submitButtonText}</Button>
  </Form>
);
