import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { useHistory } from "react-router-dom";
import { Button, Form, TextArea } from "semantic-ui-react";

import Recipe from "domain-objects/recipe";
import { updateRecipe } from "redux/actions/recipe";

const UpdateRecipe = ({ updateRecipe, recipe }) => {
  const [stateRecipe, setRecipe] = useState(new Recipe());
  const history = useHistory();
  useEffect(() => {
    setRecipe(recipe);
  }, [recipe, setRecipe]);

  return (
    <Form
      onSubmit={async (event) => {
        event.preventDefault();
        try {
          await updateRecipe(stateRecipe);
          history.push("/recipes");
        } catch (error) {
          alert(`Something went wrong: ${error.message}`);
        }
      }}
    >
      <Form.Field>
        <label htmlFor="name">Recipe</label>
        <TextArea
          rows={20}
          placeholder="Recipe"
          name="recipe"
          id="recipe"
          value={stateRecipe.recipe}
          onChange={(event) =>
            setRecipe(stateRecipe.update(event.target.name, event.target.value))
          }
        />
      </Form.Field>
      <Button type="submit">Update recipe</Button>
    </Form>
  );
};

const mapStateToProps = ({ recipe }) => ({ recipe });

const mapDispatchToProps = { updateRecipe };

export default connect(mapStateToProps, mapDispatchToProps)(UpdateRecipe);
