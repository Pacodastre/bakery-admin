import React from "react";
import { connect } from "react-redux";
import { Button, Form, Table } from "semantic-ui-react";

const SpecialOfferForm = ({
  productManager,
  stateSpecialOffer,
  setSpecialOffer,
  onSubmit,
}) => (
  <Form onSubmit={onSubmit}>
    <Form.Field>
      <label htmlFor="name">Name</label>
      <input
        type="text"
        name="name"
        onChange={(event) =>
          setSpecialOffer(
            stateSpecialOffer.update(event.target.name, event.target.value)
          )
        }
        value={stateSpecialOffer.name}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="name">Image</label>
      <input
        type="text"
        name="image"
        onChange={(event) =>
          setSpecialOffer(
            stateSpecialOffer.update(event.target.name, event.target.value)
          )
        }
        value={stateSpecialOffer.image}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="name">Price</label>
      <input
        type="text"
        name="price"
        onChange={(event) =>
          setSpecialOffer(
            stateSpecialOffer.update(event.target.name, event.target.value)
          )
        }
        value={stateSpecialOffer.price || ""}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="name">Order</label>
      <input
        type="text"
        name="order"
        onChange={(event) =>
          setSpecialOffer(
            stateSpecialOffer.update(event.target.name, event.target.value)
          )
        }
        value={stateSpecialOffer.order || ""}
      />
    </Form.Field>
    <Form.Field>
      <label htmlFor="name">Products</label>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Quantity</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {productManager.items.map((product) => {
            const item = stateSpecialOffer
              .getProducts(productManager)
              .groupProducts()
              .find(
                ({ quantity, product: specialOfferProduct }) =>
                  specialOfferProduct.id === product.id
              );
            return (
              <Table.Row key={product.key}>
                <Table.Cell>{product.name}</Table.Cell>
                <Table.Cell>{item ? item.quantity : 0}</Table.Cell>
                <Table.Cell>
                  <Button
                    onClick={(event) => {
                      event.preventDefault();
                      setSpecialOffer(stateSpecialOffer.addProduct(product.id));
                    }}
                  >
                    +1
                  </Button>
                  <Button
                    onClick={(event) => {
                      event.preventDefault();
                      setSpecialOffer(
                        stateSpecialOffer.removeProduct(product.id)
                      );
                    }}
                  >
                    -1
                  </Button>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </Form.Field>
    <Button type="submit">Create Special Offer</Button>
  </Form>
);

const mapStateToProps = ({ productManager }) => ({
  productManager,
});

export default connect(mapStateToProps)(SpecialOfferForm);
