import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import { updateSpecialOffer } from "redux/actions/special-offer";
import SpecialOffer from "domain-objects/special-offer";
import SpecialOfferForm from "components/special-offers/form";

const UpdateSpecialOffer = ({ updateSpecialOffer, specialOffer }) => {
  const [stateSpecialOffer, setSpecialOffer] = useState(new SpecialOffer());
  const history = useHistory();
  useEffect(() => {
    setSpecialOffer(specialOffer);
  }, [setSpecialOffer, specialOffer]);

  return (
    <SpecialOfferForm
      onSubmit={async (event) => {
        event.preventDefault();
        await updateSpecialOffer(stateSpecialOffer);
        history.push("/special-offers");
      }}
      stateSpecialOffer={stateSpecialOffer}
      setSpecialOffer={setSpecialOffer}
    />
  );
};

const mapStateToProps = ({ specialOffer }) => ({ specialOffer });

const mapDispatchToProps = { updateSpecialOffer };

export default connect(mapStateToProps, mapDispatchToProps)(UpdateSpecialOffer);
