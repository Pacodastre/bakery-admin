import React from "react";
import { connect } from "react-redux";
import { Button, Checkbox, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getSpecialOffers } from "redux/actions/special-offer-manager";
import { updateSpecialOffer } from "redux/actions/special-offer";

const SpecialOfferList = ({
  specialOfferManager,
  productManager,
  getSpecialOffers,
  updateSpecialOffer,
}) => (
  <Table>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Name</Table.HeaderCell>
        <Table.HeaderCell>Price</Table.HeaderCell>
        <Table.HeaderCell>Products</Table.HeaderCell>
        <Table.HeaderCell>Order</Table.HeaderCell>
        <Table.HeaderCell></Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {specialOfferManager.items.map((specialOffer) => (
        <Table.Row key={specialOffer.id}>
          <Table.Cell>{specialOffer.name}</Table.Cell>
          <Table.Cell>{specialOffer.price}</Table.Cell>
          <Table.Cell>
            <ul>
              {productManager.items.length > 0 &&
                specialOffer
                  .getProducts(productManager)
                  .groupProducts()
                  .map(({ quantity, product }) => (
                    <li key={product.id}>
                      {product.name} x{quantity}
                    </li>
                  ))}
            </ul>
          </Table.Cell>
          <Table.Cell>{specialOffer.order}</Table.Cell>
          <Table.Cell>
            <Checkbox
              toggle
              onChange={async (event, data) => {
                await updateSpecialOffer(
                  specialOffer.update("disabled", !data.checked)
                );
                getSpecialOffers();
              }}
              checked={!specialOffer.disabled}
            />
          </Table.Cell>
          <Table.Cell>
            <Link to={`/special-offers/${specialOffer.id}/edit`}>
              <Button>Edit</Button>
            </Link>
          </Table.Cell>
        </Table.Row>
      ))}
    </Table.Body>
  </Table>
);

const mapStateToProps = ({ specialOfferManager, productManager }) => ({
  specialOfferManager,
  productManager,
});

const mapDispatchToProps = { updateSpecialOffer, getSpecialOffers };

export default connect(mapStateToProps, mapDispatchToProps)(SpecialOfferList);
