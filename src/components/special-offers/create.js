import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import { createSpecialOffer } from "redux/actions/special-offer";
import SpecialOffer from "domain-objects/special-offer";
import SpecialOfferForm from "components/special-offers/form";

const CreateSpecialOffer = ({ createSpecialOffer }) => {
  const [stateSpecialOffer, setSpecialOffer] = useState(new SpecialOffer());
  const history = useHistory();

  return (
    <SpecialOfferForm
      onSubmit={async (event) => {
        event.preventDefault();
        await createSpecialOffer(stateSpecialOffer);
        history.push("/special-offers");
      }}
      stateSpecialOffer={stateSpecialOffer}
      setSpecialOffer={setSpecialOffer}
    />
  );
};

const mapDispatchToProps = { createSpecialOffer };

export default connect(null, mapDispatchToProps)(CreateSpecialOffer);
