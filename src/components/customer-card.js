import React from "react";
import { Button, Card, Grid, Icon, List } from "semantic-ui-react";

const CustomerCard = ({ customer, isDelivery }) => (
  <Card fluid>
    <Card.Content>
      <Card.Header>
        {customer.first_name} {customer.last_name}
      </Card.Header>
      <Grid columns={2} stackable>
        <Grid.Column>
          <List divided>
            <List.Item>
              <List.Content>
                <Icon name="mail" /> {customer.email}
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Content>
                <Icon name="phone" /> {customer.phone}
              </List.Content>
            </List.Item>
          </List>
        </Grid.Column>
        {isDelivery && (
          <Grid.Column>
            <List divided>
              <List.Item>
                <List.Content>
                  <Icon name="address card" />
                  {customer.address1}
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Content>
                  <Icon name="address card" />
                  {customer.address2}
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Content>{customer.city}</List.Content>
              </List.Item>
              <List.Item>
                <List.Content>{customer.county}</List.Content>
              </List.Item>
            </List>
          </Grid.Column>
        )}
      </Grid>
      {isDelivery && (
        <a
          href={`https://maps.google.com/?q=${customer.address1},%20${customer.address2},%20${customer.city},%20${customer.county}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button>View address on google maps</Button>
        </a>
      )}
    </Card.Content>
  </Card>
);

export default CustomerCard;
