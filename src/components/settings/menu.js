import React from "react";
import { Grid, Menu } from "semantic-ui-react";
import { Link, useLocation } from "react-router-dom";

const SettingsMenu = ({ children }) => {
  const { pathname } = useLocation();

  return (
    <Grid columns={2}>
      <Grid.Row>
        <Grid.Column width={3}>
          <Menu fluid vertical tabular>
            <Menu.Item
              name="allergens"
              active={pathname.startsWith("/settings/allergens")}
              as={Link}
              to="/settings/allergens"
            >
              Allergens
            </Menu.Item>
            <Menu.Item
              name="configuration"
              active={pathname === "/settings/configuration"}
              as={Link}
              to="/settings/configuration"
            >
              Configuration
            </Menu.Item>
            <Menu.Item
              name="about"
              active={pathname === "/settings/about"}
              as={Link}
              to="/settings/about"
            >
              About page
            </Menu.Item>
          </Menu>
        </Grid.Column>
        <Grid.Column width={13}>{children}</Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default SettingsMenu;
