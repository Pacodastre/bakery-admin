import React, { useState } from "react";
import { connect } from "react-redux";

import { useHistory } from "react-router-dom";

import Product from "domain-objects/product";
import { createProduct } from "redux/actions/product";
import ProductForm from "components/products/form";

const CreateProduct = ({ createProduct, allergenManager }) => {
  const [product, setProduct] = useState(new Product());
  const history = useHistory();

  return (
    <ProductForm
      product={product}
      setProduct={setProduct}
      onSubmit={async (event) => {
        event.preventDefault();
        try {
          await createProduct(product);
          history.push("/products");
        } catch (error) {
          alert(`Something went wrong: ${error.message}`);
        }
      }}
    />
  );
};

const mapDispatchToProps = { createProduct };

export default connect(null, mapDispatchToProps)(CreateProduct);
