import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Form } from "semantic-ui-react";
import { useHistory } from "react-router-dom";

import { uploadImage } from "redux/actions/image";

const CreateImage = ({ uploadImage = () => {}, image }) => {
  const [stateImage, setImage] = useState(null);
  const history = useHistory();
  return (
    <Form
      onSubmit={async (event) => {
        event.preventDefault();
        await uploadImage(stateImage);
        history.push("/images");
      }}
      encType="multipart/form-data"
    >
      <Form.Field>
        <label htmlFor="image">Image</label>
        <input
          type="file"
          name="image"
          onChange={(event) => setImage(event.target.files[0])}
        />
      </Form.Field>
      <Button type="submit">Upload</Button>
    </Form>
  );
};

const mapStateToProps = ({ image }) => ({
  image,
});

const mapDispatchToProps = { uploadImage };

export default connect(mapStateToProps, mapDispatchToProps)(CreateImage);
