import React from "react";
import { connect } from "react-redux";
import { Button, Icon, Image, Table } from "semantic-ui-react";

import { getImages } from "redux/actions/image-manager";
import { deleteImage } from "redux/actions/image";

const ImageList = ({ imageManager, deleteImage, getImages }) => {
  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Image</Table.HeaderCell>
          <Table.HeaderCell>URL</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {imageManager.items.map((image) => (
          <Table.Row key={image.id}>
            <Table.Cell>
              <Image size="small" fluid src={image.url} />
            </Table.Cell>
            <Table.Cell>{image.url}</Table.Cell>
            <Table.Cell>
              <Button
                onClick={async (event) => {
                  await deleteImage(image.id);
                  getImages();
                }}
              >
                <Icon name="delete" />
                Delete
              </Button>
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

const mapStateToProps = ({ imageManager }) => ({
  imageManager,
});

const mapDispatchToProps = {
  deleteImage,
  getImages,
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageList);
