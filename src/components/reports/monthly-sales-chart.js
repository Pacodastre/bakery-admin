import React from "react";
import {
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import { MONTHS } from "utils/date";

const MonthlySalesChart = ({ orderManager }) => {
  const data = MONTHS.map((month) => {
    if (orderManager.filterMonth(month).items.length)
      return {
        name: month,
        "Amount Sold": orderManager.filterMonth(month).calculateTotalSold(),
      };
    return { name: month };
  });
  return (
    <LineChart
      width={1100}
      height={300}
      data={data}
      margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
    >
      <Line type="monotone" dataKey="Amount Sold" stroke="#8884d8" />
      <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
    </LineChart>
  );
};

export default MonthlySalesChart;
