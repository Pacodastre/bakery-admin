import React from "react";
import { connect } from "react-redux";
import {
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import { MONTHS } from "utils/date";

const MonthlySalesByProductChart = ({
  orderManager,
  productManager,
  specialOfferManager,
}) => {
  const colours = [
    "#951F1F",
    "#CEBD30",
    "#2B6007",
    "#78EC29",
    "#1B773B",
    "#35D2AD",
    "#1B6184",
    "#2D3564",
    "#621F91",
    "#EB00FF",
    "#803A57",
    "#751720",
  ];
  const data = MONTHS.map((month) => {
    return productManager.items
      .map((product) => {
        return orderManager
          .filterMonth(month)
          .groupByProduct(specialOfferManager)
          .filter(({ product_id }) => product.id === product_id)
          .map(({ product_id, quantity }) => ({
            [productManager.getProduct(product_id).name]: quantity,
          }));
      })
      .flat()
      .reduce((accumulator, monthItem) => ({ ...accumulator, ...monthItem }), {
        name: month,
      });
  });
  return (
    <LineChart
      width={1100}
      height={300}
      data={data}
      margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
    >
      {productManager.items.map((product, index) => (
        <Line type="monotone" dataKey={product.name} stroke={colours[index]} />
      ))}
      <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
    </LineChart>
  );
};

const mapStateToProps = ({ productManager, specialOfferManager }) => ({
  productManager,
  specialOfferManager,
});

export default connect(mapStateToProps)(MonthlySalesByProductChart);
