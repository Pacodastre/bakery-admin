import React from "react";
import { connect } from "react-redux";
import { Table } from "semantic-ui-react";

const MessageList = ({ messageManager }) => {
  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell collapsing>Sent on</Table.HeaderCell>
          <Table.HeaderCell>Name</Table.HeaderCell>
          <Table.HeaderCell>Email</Table.HeaderCell>
          <Table.HeaderCell>Message</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {messageManager.items.map((message) => (
          <Table.Row key={message.id}>
            <Table.Cell>{message.renderInsertedAt()}</Table.Cell>
            <Table.Cell>{message.name}</Table.Cell>
            <Table.Cell>{message.email}</Table.Cell>
            <Table.Cell>{message.message}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

const mapStateToProps = ({ messageManager, productManager }) => ({
  messageManager,
  productManager,
});

export default connect(mapStateToProps)(MessageList);
