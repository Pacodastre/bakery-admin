import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams, useHistory } from "react-router-dom";

import {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
  updateOrder,
} from "redux/actions/order";
import Order from "domain-objects/order";

import OrderForm from "components/orders/form";

const EditOrder = ({
  order,
  orderStatusManager,
  getOrder,
  productManager,
  clearOrder,
  approveOrder,
  declineOrder,
  updateOrder,
}) => {
  const history = useHistory();
  const { id } = useParams();
  const [stateOrder, setStateOrder] = useState(new Order());

  useEffect(() => {
    getOrder(id);
    // return () => clearOrder();
  }, [getOrder, id, clearOrder]);
  useEffect(() => {
    setStateOrder(order);
  }, [setStateOrder, order]);

  return (
    <OrderForm
      onSubmit={(event) => {
        event.preventDefault();
        updateOrder(stateOrder);
        history.push(`/orders/${order.id}`);
      }}
      stateOrder={stateOrder}
      setStateOrder={setStateOrder}
    />
  );
};

const mapStateToProps = ({ order, orderStatusManager, productManager }) => ({
  order,
  orderStatusManager,
  productManager,
});

const mapDispatchToProps = {
  getOrder,
  clearOrder,
  approveOrder,
  declineOrder,
  updateOrder,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditOrder);
