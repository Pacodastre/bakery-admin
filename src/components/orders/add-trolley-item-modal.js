import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Header, Modal, Table } from "semantic-ui-react";

const AddTrolleyItemModal = ({
  productManager,
  stateTrolley,
  setStateTrolley,
}) => {
  const [open, setOpen] = useState(false);
  return (
    <Modal
      trigger={<Button type="button">Add item</Button>}
      onOpen={() => setOpen(true)}
      onClose={() => setOpen(false)}
      open={open}
    >
      <Header>Add item</Header>
      <Modal.Content>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Product</Table.HeaderCell>
              <Table.HeaderCell>Quantity</Table.HeaderCell>
              <Table.HeaderCell></Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {productManager.items.map((product) => (
              <Table.Row key={product.id}>
                <Table.Cell>{product.name}</Table.Cell>
                <Table.Cell>
                  {stateTrolley.findTrolleyItemByProduct(product).quantity}
                </Table.Cell>
                <Table.Cell>
                  <Button
                    onClick={(event) => {
                      event.preventDefault();
                      setStateTrolley(stateTrolley.addOneItem(product));
                    }}
                  >
                    +
                  </Button>
                  <Button
                    onClick={(event) => {
                      event.preventDefault();
                      setStateTrolley(stateTrolley.removeOneItem(product));
                    }}
                  >
                    -
                  </Button>
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <Modal.Actions>
          <Button onClick={() => setOpen(false)}>Close</Button>
        </Modal.Actions>
      </Modal.Content>
    </Modal>
  );
};

const mapStateToProps = ({ productManager }) => ({
  productManager,
});

export default connect(mapStateToProps)(AddTrolleyItemModal);
