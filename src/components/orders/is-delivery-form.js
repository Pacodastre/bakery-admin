import React from "react";
import { Icon, Menu } from "semantic-ui-react";

const IsDeliveryForm = ({ order, setIsDelivery }) => {
  return (
    <Menu compact id="isDelivery">
      <Menu.Item
        active={order.is_delivery === true}
        onClick={(event) => setIsDelivery(true)}
      >
        <Icon name="bicycle" />
        Delivery
      </Menu.Item>
      <Menu.Item
        active={order.is_delivery === false}
        onClick={(event) => setIsDelivery(false)}
      >
        <Icon name="shopping bag" />
        Collection
      </Menu.Item>
    </Menu>
  );
};

export default IsDeliveryForm;
