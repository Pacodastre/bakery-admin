import React from "react";
import { connect } from "react-redux";
import { Button, Form, Select, Label } from "semantic-ui-react";

import { updateOrder } from "redux/actions/order";

const OrderStatusForm = ({
  stateOrder,
  order,
  setStateOrder,
  updateOrder,
  orderStatusManager,
}) => {
  const colours = {
    waiting: "blue",
    confirmed: "orange",
    approved: "green",
    declined: "red",
  };
  const options = orderStatusManager.items.map((orderStatus) => ({
    key: orderStatus.id,
    value: orderStatus.id,
    text: (
      <Label color={colours[orderStatus.status]}>{orderStatus.status}</Label>
    ),
  }));

  return (
    <Form
      onSubmit={(event) => {
        event.preventDefault();
        updateOrder(stateOrder);
      }}
    >
      <Form.Field inline>
        <Select
          onChange={(event, { value }) =>
            setStateOrder(order.update("status_id", value))
          }
          options={options}
          value={stateOrder.status_id}
        />
        <Button
          primary
          disabled={order.status_id === stateOrder.status_id}
          type="submit"
        >
          Save
        </Button>
      </Form.Field>
    </Form>
  );
};

const mapStateToProps = ({ orderStatusManager }) => ({
  orderStatusManager,
});
const mapDispatchToProps = { updateOrder };

export default connect(mapStateToProps, mapDispatchToProps)(OrderStatusForm);
