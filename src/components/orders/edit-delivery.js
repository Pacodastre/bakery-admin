import React from "react";
import { Form, Header } from "semantic-ui-react";

const EditDelivery = ({ delivery, stateDelivery, setStateDelivery }) => {
  return (
    <>
      <Header as="h3">Delivery details</Header>
      <Form.Field>
        <label>Delivery Date (Format: YYYY-MM-DD)</label>
        <input
          placeholder="delivery_date"
          name="delivery_date"
          id="delivery_date"
          value={stateDelivery.delivery_date || ""}
          onChange={(event) =>
            setStateDelivery(
              stateDelivery.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Time Slot</label>
        <input
          placeholder="time_slot"
          name="time_slot"
          id="time_slot"
          value={stateDelivery.time_slot || ""}
          onChange={(event) =>
            setStateDelivery(
              stateDelivery.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
    </>
  );
};

export default EditDelivery;
