import React from "react";
import { connect } from "react-redux";
import { Header, Table } from "semantic-ui-react";

const OrderSummary = ({ summary, productManager }) => (
  <div>
    <Header as="h2">Order Summary</Header>
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Product</Table.HeaderCell>
          <Table.HeaderCell>Quantity</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {summary.map((item) => (
          <Table.Row>
            <Table.Cell>
              {productManager.getProduct(item.product_id).name}
            </Table.Cell>
            <Table.Cell>{item.quantity}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  </div>
);
const mapStateToProps = ({ productManager }) => ({ productManager });

export default connect(mapStateToProps)(OrderSummary);
