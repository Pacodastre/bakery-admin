import React from "react";
import { Form, Header } from "semantic-ui-react";

const EditCustomer = ({ customer, stateCustomer, setStateCustomer }) => {
  return (
    <>
      <Header as="h3">Customer details</Header>
      <Form.Field>
        <label>First Name</label>
        <input
          placeholder="First Name"
          name="first_name"
          id="first_name"
          value={stateCustomer.first_name}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Last Name</label>
        <input
          placeholder="Last Name"
          name="last_name"
          id="last_name"
          value={stateCustomer.last_name}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Phone</label>
        <input
          placeholder="Phone"
          name="phone"
          id="phone"
          value={stateCustomer.phone}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Email</label>
        <input
          placeholder="email"
          name="email"
          id="email"
          value={stateCustomer.email}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Address 1</label>
        <input
          placeholder="address1"
          name="address1"
          id="address1"
          value={stateCustomer.address1}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
      <Form.Field>
        <label>Address 2</label>
        <input
          placeholder="address2"
          name="address2"
          id="address2"
          value={stateCustomer.address2}
          onChange={(event) =>
            setStateCustomer(
              stateCustomer.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
    </>
  );
};

export default EditCustomer;
