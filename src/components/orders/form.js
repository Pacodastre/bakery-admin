import React from "react";
import { connect } from "react-redux";
import { Button, Form } from "semantic-ui-react";

import EditDelivery from "components/orders/edit-delivery";
import IsDeliveryForm from "components/orders/is-delivery-form";
import EditTrolley from "components/orders/edit-trolley";
import EditPayment from "components/orders/edit-payment";
import EditCustomer from "components/orders/edit-customer";
import { getOrder, clearOrder } from "redux/actions/order";
import AddTrolleyItemModal from "components/orders/add-trolley-item-modal";

const OrderForm = ({
  order,
  clearOrder,
  onSubmit,
  setStateOrder,
  stateOrder,
}) => {
  const setStateTrolley = (trolley) =>
    setStateOrder(stateOrder.update("trolley", trolley));
  return (
    <Form onSubmit={onSubmit}>
      <IsDeliveryForm
        setIsDelivery={(value) =>
          setStateOrder(stateOrder.updateIsDelivery(value))
        }
        order={stateOrder}
      />
      <br />
      <br />
      <AddTrolleyItemModal
        setStateTrolley={setStateTrolley}
        stateTrolley={stateOrder.trolley}
      />
      <EditTrolley
        trolley={order.trolley}
        setStateTrolley={setStateTrolley}
        stateTrolley={stateOrder.trolley}
      />
      <EditDelivery
        delivery={stateOrder.delivery}
        setStateDelivery={(delivery) =>
          setStateOrder(stateOrder.update("delivery", delivery))
        }
        stateDelivery={stateOrder.delivery}
      />
      <EditCustomer
        customer={stateOrder.customer}
        setStateCustomer={(customer) =>
          setStateOrder(stateOrder.update("customer", customer))
        }
        stateCustomer={stateOrder.customer}
      />
      <EditPayment
        payment={stateOrder.payment}
        setStatePayment={(payment) =>
          setStateOrder(stateOrder.update("payment", payment))
        }
        statePayment={stateOrder.payment}
      />
      <Button type="submit" primary disabled={order.isSame(stateOrder)}>
        Edit
      </Button>
    </Form>
  );
};

const mapStateToProps = ({ order }) => ({ order });

const mapDispatchToProps = { clearOrder, getOrder };

export default connect(mapStateToProps, mapDispatchToProps)(OrderForm);
