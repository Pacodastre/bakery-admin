import React from "react";
import { connect } from "react-redux";
import { Label, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

const StatusTabBar = ({ orderManager, status, orderStatusManager }) => (
  <Menu tabular fluid stackable>
    <Menu.Item
      name="waiting"
      color="blue"
      style={{ color: "blue" }}
      as={Link}
      to="/orders/waiting"
      active={status === "waiting"}
    >
      Waiting
      <Label color="blue">
        {
          orderManager.filterStatus(
            orderStatusManager.getByStatus("waiting").id
          ).items.length
        }
      </Label>
    </Menu.Item>
    <Menu.Item
      name="confirmed"
      color="orange"
      style={{ color: "orange" }}
      as={Link}
      to="/orders/confirmed"
      active={status === "confirmed"}
    >
      Confirmed
      <Label color="orange">
        {
          orderManager.filterStatus(
            orderStatusManager.getByStatus("confirmed").id
          ).items.length
        }
      </Label>
    </Menu.Item>
    <Menu.Item
      name="approved"
      color="green"
      style={{ color: "green" }}
      as={Link}
      to="/orders/approved"
      active={status === "approved"}
    >
      Approved
      <Label color="green">
        {
          orderManager.filterStatus(
            orderStatusManager.getByStatus("approved").id
          ).items.length
        }
      </Label>
    </Menu.Item>
    <Menu.Item
      name="declined"
      color="red"
      style={{ color: "red" }}
      as={Link}
      to="/orders/declined"
      active={status === "declined"}
    >
      Declined
      <Label color="red">
        {
          orderManager.filterStatus(
            orderStatusManager.getByStatus("declined").id
          ).items.length
        }
      </Label>
    </Menu.Item>
    <Menu.Item
      name="delivered"
      as={Link}
      to="/orders/delivered"
      active={status === "delivered"}
    >
      Delivered
      <Label>
        {
          orderManager.filterStatus(
            orderStatusManager.getByStatus("delivered").id
          ).items.length
        }
      </Label>
    </Menu.Item>
  </Menu>
);

const mapStateToProps = ({ orderManager, orderStatusManager }) => ({
  orderManager,
  orderStatusManager,
});

export default connect(mapStateToProps)(StatusTabBar);
