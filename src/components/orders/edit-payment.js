import React from "react";
import { Form, Header } from "semantic-ui-react";

const EditPayment = ({ statePayment, setStatePayment }) => {
  return (
    <>
      <Header as="h3">Payment details</Header>
      <Form.Radio
        checked={statePayment.type === "cash"}
        value="cash"
        onChange={(event, { name, value }) =>
          setStatePayment(statePayment.update(name, value))
        }
        name="type"
        label="Cash"
      />
      <Form.Radio
        checked={statePayment.type === "contactless"}
        value="contactless"
        onChange={(event, { name, value }) =>
          setStatePayment(statePayment.update(name, value))
        }
        name="type"
        label="Contactless"
      />
      <Form.Field>
        <label>Transaction code</label>
        <input
          placeholder="Transaction Code"
          name="transaction_code"
          id="transaction_code"
          value={statePayment.transaction_code}
          onChange={(event) =>
            setStatePayment(
              statePayment.update(event.target.name, event.target.value)
            )
          }
        />
      </Form.Field>
    </>
  );
};

export default EditPayment;
