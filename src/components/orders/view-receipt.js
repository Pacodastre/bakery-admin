import React from "react";
import { connect } from "react-redux";
import { Container, Header, Image } from "semantic-ui-react";

import Placeholder from "components/placeholder";
import CustomerReceipt from "components/customer-receipt";
import { TrolleySmall } from "components/trolleys/trolley-small";
import { AllergenInfo } from "components/allergens/allergen-info";

export const ViewOrderReceipt = ({
  order,
  productManager,
  allergenManager,
  specialOfferManager,
  settingManager,
}) => {
  if (!order.id) {
    return <Placeholder columns={2} rows={15} />;
  }
  return (
    <Container>
      <Image
        src={settingManager.getByName("app_logo").value}
        size="small"
        style={{
          display: "block",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      />
      <Header as="h2" icon textAlign="center" style={{ marginTop: "0.5em" }}>
        <Header.Content>
          {settingManager.getByName("bakery_name").value}
        </Header.Content>
        <Header sub>Artisan Bread and Pastries</Header>
      </Header>
      <Header as="h2" textAlign="center">
        Thank you for you order!
      </Header>
      <Header as="h3" textAlign="center">
        We hope you will enjoy it!
      </Header>
      <Header as="h3">
        <Header.Content>Order #{order.id}</Header.Content>
        <Header sub>Order placed on: {order.formatField("inserted_at")}</Header>
      </Header>
      <TrolleySmall
        trolley={order.trolley}
        productManager={productManager}
        allergenManager={allergenManager}
        specialOfferManager={specialOfferManager}
      />
      <AllergenInfo
        trolley={order.trolley}
        productManager={productManager}
        specialOfferManager={specialOfferManager}
        allergenManager={allergenManager}
      />
      <CustomerReceipt
        delivery={order.delivery}
        customer={order.customer}
        isDelivery={order.is_delivery}
        payment={order.payment}
      />
    </Container>
  );
};

const mapStateToProps = ({
  order,
  allergenManager,
  productManager,
  specialOfferManager,
  settingManager,
}) => ({
  order,
  allergenManager,
  productManager,
  specialOfferManager,
  settingManager,
});

export default connect(mapStateToProps)(ViewOrderReceipt);
