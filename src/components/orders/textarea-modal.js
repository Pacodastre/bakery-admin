import React, { useState } from "react";
import { Button, Header, Modal, TextArea } from "semantic-ui-react";

const TextAreaModal = ({
  trigger,
  action,
  actionButtonColor = "green",
  content,
  title,
  actionButtonText,
}) => {
  const [open, setOpen] = useState(false);
  const [textarea, setTextarea] = useState("");

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      trigger={trigger}
    >
      <Header icon>{title}</Header>
      <Modal.Content>
        <p>{content}</p>
        <TextArea
          onChange={(event) => setTextarea(event.target.value)}
          value={textarea}
        ></TextArea>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => setOpen(false)}>Cancel</Button>
        <Button
          color={actionButtonColor}
          onClick={async () => {
            await action(textarea);
            setOpen(false);
          }}
        >
          {actionButtonText}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default TextAreaModal;
