import React from "react";
import { connect } from "react-redux";
import { Button, Icon, List, Table } from "semantic-ui-react";
import { Link } from "react-router-dom";

import TextAreaModal from "components/orders/textarea-modal";
import { getOrders, clearOrders } from "redux/actions/order-manager";
import {
  updateOrder,
  clearOrder,
  approveOrder,
  declineOrder,
} from "redux/actions/order";
import { downloadReport } from "redux/actions/order-report";

const OrderList = ({
  orderManager,
  productManager,
  status,
  dateFilter,
  updateOrder,
  approveOrder,
  declineOrder,
  getOrders,
  orderStatusManager,
  specialOfferManager,
  downloadReport,
}) => {
  return (
    <div>
      <Button onClick={() => downloadReport()}>Download report</Button>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell></Table.HeaderCell>
            <Table.HeaderCell>Customer</Table.HeaderCell>
            <Table.HeaderCell>Delivery</Table.HeaderCell>
            <Table.HeaderCell>Trolley</Table.HeaderCell>
            <Table.HeaderCell style={{ width: 150 }}>Message</Table.HeaderCell>
            <Table.HeaderCell>Address</Table.HeaderCell>
            <Table.HeaderCell>Is Delivery?</Table.HeaderCell>
            <Table.HeaderCell>Payment</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {orderManager.items.map((order) => (
            <Table.Row key={order.id}>
              <Table.Cell>
                <Link to={`/orders/${order.id}`}>
                  <Button>View</Button>
                </Link>
              </Table.Cell>
              <Table.Cell>
                {order.customer.first_name} {order.customer.last_name}
              </Table.Cell>
              <Table.Cell>
                {order.delivery.formatField("delivery_date")} at{" "}
                {order.delivery.time_slot}
              </Table.Cell>
              <Table.Cell>
                <List>
                  {order.trolley.items.map((trolleyItem) => {
                    const product =
                      trolleyItem.product_id &&
                      productManager.getProduct(trolleyItem.product_id);
                    const specialOffer =
                      trolleyItem.special_offer_id &&
                      specialOfferManager.getSpecialOffer(
                        trolleyItem.special_offer_id
                      );
                    return (
                      <List.Item key={trolleyItem.id}>
                        {trolleyItem.isProduct()
                          ? product.name
                          : specialOffer.name}{" "}
                        x{trolleyItem.quantity}
                      </List.Item>
                    );
                  })}
                  <List.Item>
                    Total <strong>€{order.trolley.calculateTotal()}</strong>
                  </List.Item>
                </List>
              </Table.Cell>
              <Table.Cell>
                <cite>
                  {order.delivery.message || (
                    <span style={{ color: "grey" }}>No Message</span>
                  )}
                </cite>
              </Table.Cell>
              <Table.Cell>
                {order.customer.address1}
                <br />
                {order.customer.address2}
              </Table.Cell>
              <Table.Cell textAlign="center">
                {order.is_delivery ? (
                  <Icon name="bicycle" />
                ) : (
                  <Icon name="shopping bag" />
                )}
              </Table.Cell>
              <Table.Cell textAlign="center">
                {order.payment.type === "contactless" ? (
                  <Icon name="credit card outline" />
                ) : (
                  <Icon name="money" />
                )}
              </Table.Cell>
              <Table.Cell>
                {status === "confirmed" && (
                  <>
                    <TextAreaModal
                      title="Approve order"
                      content="Approve text"
                      trigger={<Button color="green">Approve</Button>}
                      action={async (message) => {
                        await approveOrder(order.update("message", message));
                        getOrders();
                      }}
                      actionButtonText="Approve"
                    />
                    <TextAreaModal
                      actionButtonColor="red"
                      actionButtonText="Decline"
                      title="Decline order"
                      content="Reason to decline order"
                      trigger={<Button color="red">Decline</Button>}
                      action={async (message) => {
                        await declineOrder(order.update("message", message));
                        getOrders();
                      }}
                    />
                  </>
                )}
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </div>
  );
};

const mapStateToProps = (
  { orderManager, productManager, orderStatusManager, specialOfferManager },
  ownProps
) => ({
  orderManager: orderManager
    .filterStatus(orderStatusManager.getByStatus(ownProps.status).id)
    .filterDate(ownProps.dateFilter),
  productManager,
  orderStatusManager,
  specialOfferManager,
});

const mapDispatchToProps = {
  getOrders,
  updateOrder,
  approveOrder,
  declineOrder,
  clearOrder,
  clearOrders,
  downloadReport,
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
