import Image from "domain-objects/image";

export default class ImageManager {
  constructor(items = []) {
    this.items = items;
  }
}

export function createImageManager(items) {
  return new ImageManager(items.map((image) => new Image(image)));
}
