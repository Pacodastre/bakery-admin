export default class Allergen {
  constructor({ id, name, image = "" } = {}) {
    this.id = id;
    this.name = name;
    this.image = image;
  }

  update(field, value) {
    return new Allergen({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      id: this.id,
      name: this.name,
      image: this.image,
    };
  }
}
