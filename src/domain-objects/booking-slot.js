import moment from "moment";

export default class BookingSlot {
  constructor({ id, date, time_slot, order_id, is_delivery = true } = {}) {
    this.id = id;
    this.date = date;
    this.time_slot = time_slot;
    this.order_id = order_id;
    this.is_delivery = is_delivery;
  }

  update(field, value) {
    return new BookingSlot({ ...this, [field]: value });
  }

  toMoment(field) {
    return moment(this[field]);
  }

  renderJSON() {
    return {
      id: this.id,
      date: this.date,
      time_slot: this.time_slot,
      order_id: this.order_id,
      is_delivery: this.is_delivery,
    };
  }
}
