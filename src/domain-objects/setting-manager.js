import Setting from "domain-objects/setting";

export default class SettingManager {
  constructor(items = []) {
    this.items = items;
  }

  updateSetting(name, value) {
    const setting = this.getByName(name);
    return new SettingManager([
      ...this.items.slice(0, this.items.indexOf(setting)),
      setting.update(value),
      ...this.items.slice(this.items.indexOf(setting) + 1),
    ]);
  }

  getByName(name) {
    return this.items.find((setting) => setting.name === name) || new Setting();
  }

  renderJSON() {
    return this.items.map((setting) => setting.renderJSON());
  }
}

export function createSettingManager(items) {
  return new SettingManager(items.map((setting) => new Setting(setting)));
}
