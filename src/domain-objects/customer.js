export default class Delivery {
  constructor({
    first_name = "",
    last_name = "",
    email = "",
    phone = "",
    address1 = "",
    address2 = "",
    city = "Dublin",
    county = "Dublin",
  } = {}) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.address1 = address1;
    this.address2 = address2;
    this.city = city;
    this.county = county;
  }

  isValid() {
    return (
      this.first_name !== "" &&
      this.last_name !== "" &&
      this.email !== "" &&
      this.phone !== "" &&
      this.address1 !== "" &&
      this.address2 !== "" &&
      this.city !== "" &&
      this.county !== ""
    );
  }

  isSame(delivery) {
    return (
      this.first_name === delivery.first_name &&
      this.last_name === delivery.last_name &&
      this.email === delivery.email &&
      this.phone === delivery.phone &&
      this.address1 === delivery.address1 &&
      this.address2 === delivery.address2 &&
      this.city === delivery.city &&
      this.county === delivery.county
    );
  }

  formatField(field) {
    switch (field) {
      default: {
        return this[field];
      }
    }
  }

  update(field, value) {
    return new Delivery({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      phone: this.phone,
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      county: this.county,
    };
  }
}
