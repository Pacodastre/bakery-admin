import OrderStatus from "domain-objects/order-status";

export default class OrderStatusManager {
  constructor(items = []) {
    this.items = items;
  }

  getByStatus(status) {
    return (
      this.items.find((orderStatus) => orderStatus.status === status) ||
      new OrderStatus()
    );
  }

  getById(id) {
    return (
      this.items.find((orderStatus) => orderStatus.id === id) ||
      new OrderStatus()
    );
  }
}
