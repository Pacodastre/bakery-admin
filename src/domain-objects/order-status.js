export default class OrderStatus {
  constructor({ id, status = "" } = {}) {
    this.id = id;
    this.status = status;
  }

  renderJSON() {
    return {
      id: this.id,
      status: this.status,
    };
  }
}
