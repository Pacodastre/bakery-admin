import moment from "moment";

import BookingSlot from "domain-objects/booking-slot";

export default class BookingSlotManager {
  constructor(items = []) {
    this.items = items;
  }

  filter(callback) {
    return new BookingSlotManager(this.items.filter(callback));
  }

  filterByIsDelivery(isDelivery) {
    return this.filter((bookingSlot) => bookingSlot.is_delivery === isDelivery);
  }

  getSlots(momentDate, timeSlot) {
    return new BookingSlotManager(
      this.items.filter(
        (bookingSlot) =>
          moment(bookingSlot.date).isSame(momentDate, "days") &&
          bookingSlot.time_slot === timeSlot
      )
    );
  }

  getBookedSlots() {
    return new BookingSlotManager(
      this.items.filter((bookingSlot) => bookingSlot.order_id)
    );
  }

  length() {
    return this.items.length;
  }

  addSlot(bookingSlot) {
    return new BookingSlotManager([...this.items, bookingSlot]);
  }

  removeSlot(id) {
    return new BookingSlotManager(
      this.items.filter((bookingSlot) => bookingSlot.id !== id)
    );
  }
}

export function createBookingSlotManager(items) {
  return new BookingSlotManager(
    items.map((bookingSlot) => new BookingSlot(bookingSlot))
  );
}
