import AllergenManager from "domain-objects/allergen-manager";

export default class Product {
  constructor({
    id,
    key = "",
    name = "",
    description = "",
    image = "",
    price = 0,
    type = "bread",
    order,
    disabled,
    allergens = [],
    may_contain_allergens = [],
  } = {}) {
    this.id = id;
    this.key = key;
    this.name = name;
    this.description = description;
    this.image = image;
    this.price = price;
    this.type = type;
    this.order = order;
    this.disabled = disabled;
    this.allergens = allergens;
    this.may_contain_allergens = may_contain_allergens;
  }

  getAllergens(allergenManager) {
    return new AllergenManager(
      this.allergens.map((allergen) => allergenManager.getAllergen(allergen))
    );
  }

  getMayContainAllergens(allergenManager) {
    return new AllergenManager(
      this.may_contain_allergens.map((allergen) =>
        allergenManager.getAllergen(allergen)
      )
    );
  }

  update(field, value) {
    return new Product({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      product: {
        id: this.id,
        key: this.key,
        name: this.name,
        description: this.description,
        image: this.image,
        price: this.price,
        disabled: this.disabled,
        order: this.order,
        allergens: this.allergens,
        may_contain_allergens: this.may_contain_allergens,
      },
    };
  }
}
