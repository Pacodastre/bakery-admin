export default class Payment {
  constructor({ type = "", transaction_code = "" } = {}) {
    this.type = type;
    this.transaction_code = transaction_code;
  }

  isValid() {
    return !!this.type;
  }

  isSame(payment) {
    return (
      this.type === payment.type &&
      this.transaction_code === payment.transaction_code
    );
  }

  update(name, value) {
    return new Payment({ ...this, [name]: value });
  }

  renderJSON() {
    return { type: this.type, transaction_code: this.transaction_code };
  }
}
