export default class TrolleyItem {
  constructor({
    id,
    product_id,
    special_offer_id,
    quantity = 0,
    unit_price = 0,
  } = {}) {
    this.id = id;
    this.product_id = product_id;
    this.special_offer_id = special_offer_id;
    this.quantity = quantity;
    this.unit_price = unit_price;
  }

  calculateTotal() {
    return Math.round(this.quantity * this.unit_price * 100) / 100;
  }

  getProduct(productManager) {
    return productManager.getProduct(this.product_id);
  }

  isSame(trolleyItem) {
    return (
      this.id === trolleyItem.id &&
      this.product_id === trolleyItem.product_id &&
      this.quantity === trolleyItem.quantity &&
      this.unit_price === trolleyItem.unit_price
    );
  }

  isProduct() {
    return !!this.product_id;
  }

  update(field, value) {
    return new TrolleyItem({ ...this, [field]: value });
  }

  renderJSON() {
    return {
      product_id: this.product_id,
      special_offer_id: this.special_offer_id,
      quantity: this.quantity,
      unit_price: this.unit_price,
    };
  }
}
