import TrolleyItem from "domain-objects/trolley-item";

export default class Trolley {
  constructor({ items = [], delivery_price = 3 } = {}) {
    this.items = items;
    this.delivery_price = delivery_price;
  }

  findTrolleyItemByProduct(product) {
    return (
      this.items.find((item) => item.product_id === product.id) ||
      new TrolleyItem()
    );
  }

  findTrolleyItem(id) {
    return this.items.find((item) => item.id === id);
  }

  calculateTotal() {
    // Add delivery_price if there's more than one item in the trolley
    const total =
      this.items.reduce((total, trolleyItem) => {
        return total + trolleyItem.calculateTotal();
      }, 0) + this.calculateDeliveryPrice();
    return Math.round(total * 100) / 100;
  }

  calculateDeliveryPrice() {
    return this.items.length > 0 ? parseFloat(this.delivery_price) : 0;
  }

  numberItems() {
    return this.items.reduce(
      (total, trolleyItem) => trolleyItem.quantity + total,
      0
    );
  }

  addOneItem(product) {
    if (
      !this.items.find((trolleyItem) => trolleyItem.product_id === product.id)
    ) {
      return new Trolley({
        ...this,
        items: [
          ...this.items,
          new TrolleyItem({
            product_id: product.id,
            quantity: 1,
            unit_price: product.price,
          }),
        ],
      });
    } else {
      const item = this.items.find(
        (trolleyItem) => trolleyItem.product_id === product.id
      );
      const indexOfItem = this.items.indexOf(item);
      return new Trolley({
        ...this,
        items: [
          ...this.items.slice(0, indexOfItem),
          new TrolleyItem({ ...item, quantity: item.quantity + 1 }),
          ...this.items.slice(indexOfItem + 1),
        ],
      });
    }
  }

  removeOneItem(product) {
    const item = this.items.find(
      (trolleyItem) => trolleyItem.product_id === product.id
    );
    // This happens when we try to click minus on products where quantity is 0
    if (!item) return this;
    const indexOfItem = this.items.indexOf(item);
    const quantity = item.quantity - 1 >= 0 ? item.quantity - 1 : 0;
    return new Trolley({
      ...this,
      items: [
        ...this.items.slice(0, indexOfItem),
        new TrolleyItem({ ...item, quantity }),
        ...this.items.slice(indexOfItem + 1),
      ],
    });
  }

  deleteItem(product) {
    const item = this.items.find(
      (trolleyItem) => trolleyItem.product_id === product.id
    );
    const indexOfItem = this.items.indexOf(item);
    return new Trolley({
      ...this,
      items: [
        ...this.items.slice(0, indexOfItem),
        ...this.items.slice(indexOfItem + 1),
      ],
    });
  }

  isSame(trolley) {
    return (
      this.items.every((thisItem) => {
        const item = trolley.items.find((item) => item.id === thisItem.id);
        return item && thisItem.isSame(item);
      }) && this.delivery_price === trolley.delivery_price
    );
  }

  update(field, value) {
    return new Trolley({ ...this, [field]: value });
  }

  updateTrolleyItem(trolleyItem) {
    return new Trolley({
      ...this,
      items: this.items.map(
        (item) =>
          (trolleyItem.product_id === item.product_id && trolleyItem) || item
      ),
    });
  }

  renderJSON() {
    return {
      items: this.items.map((item) => item.renderJSON()),
      delivery_price: this.delivery_price,
    };
  }
}
