import { combineReducers, applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import { productManagerReducer } from "redux/reducers/product-manager";
import { productReducer } from "redux/reducers/product";
import { orderManagerReducer } from "redux/reducers/order-manager";
import { orderReducer } from "redux/reducers/order";
import { recipeManagerReducer } from "redux/reducers/recipe-manager";
import { recipeReducer } from "redux/reducers/recipe";
import { orderStatusManagerReducer } from "redux/reducers/order-status-manager";
import { messageManagerReducer } from "redux/reducers/message-manager";
import { imageManagerReducer } from "redux/reducers/image-manager";
import { imageReducer } from "redux/reducers/image";
import { allergenManagerReducer } from "redux/reducers/allergen-manager";
import { allergenReducer } from "redux/reducers/allergen";
import { bookingSlotManagerReducer } from "redux/reducers/booking-slot-manager";
import { specialOfferManagerReducer } from "redux/reducers/special-offer-manager";
import { specialOfferReducer } from "redux/reducers/special-offer";
import { settingManagerReducer } from "redux/reducers/setting-manager";

const logger = createLogger({ collapsed: true });

const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);

const enhancers = compose(
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
);

const rootReducer = combineReducers({
  productManager: productManagerReducer,
  product: productReducer,
  orderManager: orderManagerReducer,
  order: orderReducer,
  recipeManager: recipeManagerReducer,
  recipe: recipeReducer,
  orderStatusManager: orderStatusManagerReducer,
  messageManager: messageManagerReducer,
  imageManager: imageManagerReducer,
  image: imageReducer,
  allergenManager: allergenManagerReducer,
  allergen: allergenReducer,
  bookingSlotManager: bookingSlotManagerReducer,
  specialOfferManager: specialOfferManagerReducer,
  specialOffer: specialOfferReducer,
  settingManager: settingManagerReducer,
});

export default createStoreWithMiddleware(rootReducer, enhancers);