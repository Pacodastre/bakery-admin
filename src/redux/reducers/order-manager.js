import { GET_ORDERS, CLEAR_ORDERS } from "redux/actions/action-types";

import OrderManager from "domain-objects/order-manager";

export function orderManagerReducer(state = new OrderManager(), action) {
  switch (action.type) {
    case GET_ORDERS: {
      return action.payload;
    }
    case CLEAR_ORDERS: {
      return new OrderManager();
    }
    default: {
      return state;
    }
  }
}
