import { GET_MESSAGES, CLEAR_MESSAGES } from "redux/actions/action-types";

import MessageManager from "domain-objects/message-manager";

export function messageManagerReducer(state = new MessageManager(), action) {
  switch (action.type) {
    case GET_MESSAGES: {
      return action.payload;
    }
    case CLEAR_MESSAGES: {
      return new MessageManager();
    }
    default: {
      return state;
    }
  }
}
