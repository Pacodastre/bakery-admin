import {
  CREATE_SPECIAL_OFFER,
  UPDATE_SPECIAL_OFFER,
  GET_SPECIAL_OFFER,
  CLEAR_SPECIAL_OFFER,
} from "redux/actions/action-types";

import SpecialOffer from "domain-objects/special-offer";

export function specialOfferReducer(state = new SpecialOffer(), action) {
  switch (action.type) {
    case GET_SPECIAL_OFFER:
    case UPDATE_SPECIAL_OFFER:
    case CREATE_SPECIAL_OFFER: {
      return action.payload;
    }
    case CLEAR_SPECIAL_OFFER: {
      return new SpecialOffer();
    }
    default: {
      return state;
    }
  }
}
