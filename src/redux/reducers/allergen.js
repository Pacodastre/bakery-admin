import {
  GET_ALLERGEN,
  CREATE_ALLERGEN,
  UPDATE_ALLERGEN,
  CLEAR_ALLERGEN,
} from "redux/actions/action-types";

import Allergen from "domain-objects/allergen";

export function allergenReducer(state = new Allergen(), action) {
  switch (action.type) {
    case GET_ALLERGEN:
    case UPDATE_ALLERGEN:
    case CREATE_ALLERGEN: {
      return action.payload;
    }
    case CLEAR_ALLERGEN: {
      return new Allergen();
    }
    default: {
      return state;
    }
  }
}
