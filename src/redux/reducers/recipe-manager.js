import { GET_RECIPES, CLEAR_RECIPES } from "redux/actions/action-types";

import RecipeManager from "domain-objects/recipe-manager";

export function recipeManagerReducer(state = new RecipeManager(), action) {
  switch (action.type) {
    case GET_RECIPES: {
      return action.payload;
    }
    case CLEAR_RECIPES: {
      return new RecipeManager();
    }
    default: {
      return state;
    }
  }
}
