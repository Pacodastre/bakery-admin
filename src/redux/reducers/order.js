import {
  GET_ORDER,
  CLEAR_ORDER,
  UPDATE_ORDER,
  CREATE_ORDER,
} from "redux/actions/action-types";

import Order from "domain-objects/order";

export function orderReducer(state = new Order(), action) {
  switch (action.type) {
    case UPDATE_ORDER:
    case CREATE_ORDER:
    case GET_ORDER: {
      return action.payload;
    }
    case CLEAR_ORDER: {
      return new Order();
    }
    default: {
      return state;
    }
  }
}
