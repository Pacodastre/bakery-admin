import {
  GET_BOOKING_SLOTS,
  CLEAR_BOOKING_SLOTS,
  ADD_BOOKING_SLOT,
  REMOVE_BOOKING_SLOT,
} from "redux/actions/action-types";

import BookingSlotManager from "domain-objects/booking-slot-manager";

export function bookingSlotManagerReducer(
  state = new BookingSlotManager(),
  action
) {
  switch (action.type) {
    case GET_BOOKING_SLOTS: {
      return action.payload;
    }
    case ADD_BOOKING_SLOT: {
      return state.addSlot(action.payload);
    }
    case REMOVE_BOOKING_SLOT: {
      return state.removeSlot(action.payload);
    }
    case CLEAR_BOOKING_SLOTS: {
      return new BookingSlotManager();
    }
    default: {
      return state;
    }
  }
}
