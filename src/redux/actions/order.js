import axios from "redux/actions/axios";
import {
  GET_ORDER,
  CLEAR_ORDER,
  UPDATE_ORDER,
  CREATE_ORDER,
} from "redux/actions/action-types";

import { createOrder as createOrderObject } from "domain-objects/order";

export function getOrder(orderId) {
  return (dispatch, getState) => {
    axios.get(`/admin/orders/${orderId}/`).then((response) => {
      dispatch({
        type: GET_ORDER,
        payload: createOrderObject(response.data.data),
      });
    });
  };
}

export function clearOrder() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ORDER });
  };
}

export function createOrder(order, callback) {
  return async (dispatch, getState) => {
    await axios
      .post(`/admin/orders/`, { order: order.renderJSON() })
      .then((response) => {
        dispatch({
          type: CREATE_ORDER,
          payload: createOrderObject(response.data.data),
        });
      });
    if (callback) {
      const createdOrder = getState().order;
      callback(createdOrder);
    }
  };
}

export function updateOrder(order) {
  return (dispatch, getState) => {
    axios
      .put(`/admin/orders/${order.id}/`, { order: order.renderJSON() })
      .then((response) => {
        dispatch({
          type: UPDATE_ORDER,
          payload: createOrderObject(response.data.data),
        });
      });
  };
}

export function approveOrder(order, callback) {
  return async (dispatch, getState) => {
    await axios
      .put(`/admin/orders/${order.id}/approve`, order.renderJSON())
      .then((response) => {
        dispatch({
          type: UPDATE_ORDER,
          payload: createOrderObject(response.data.data),
        });
        if (callback) {
          callback(getState().order);
        }
      });
  };
}

export function declineOrder(order, callback) {
  return async (dispatch, getState) => {
    await axios
      .put(`/admin/orders/${order.id}/decline`, order.renderJSON())
      .then((response) => {
        dispatch({
          type: UPDATE_ORDER,
          payload: createOrderObject(response.data.data),
        });
        if (callback) {
          callback(getState().order);
        }
      });
  };
}
