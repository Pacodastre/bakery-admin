import axios from "redux/actions/axios";
import { GET_RECIPES, CLEAR_RECIPES } from "redux/actions/action-types";

import RecipeManager from "domain-objects/recipe-manager";
import Recipe from "domain-objects/recipe";

export function getRecipes() {
  return (dispatch, getState) => {
    axios.get("/admin/recipes/").then((response) =>
      dispatch({
        type: GET_RECIPES,
        payload: new RecipeManager(
          response.data.data.map((recipe) => new Recipe(recipe))
        ),
      })
    );
  };
}

export function clearRecipes() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_RECIPES });
  };
}
