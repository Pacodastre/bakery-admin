import axios from "redux/actions/axios";
import { GET_ORDERS, CLEAR_ORDERS } from "redux/actions/action-types";

import OrderManager from "domain-objects/order-manager";
import { createOrder } from "domain-objects/order";

export function getOrders() {
  return (dispatch, getState) => {
    axios.get("/admin/orders/").then((response) =>
      dispatch({
        type: GET_ORDERS,
        payload: new OrderManager(
          response.data.data.map((order) => createOrder(order))
        ),
      })
    );
  };
}

export function clearOrders() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ORDERS });
  };
}
