import axios from "redux/actions/axios";
import { GET_ALLERGENS, CLEAR_ALLERGENS } from "redux/actions/action-types";

import { createAllergenManager } from "domain-objects/allergen-manager";

export function getAllergens() {
  return (dispatch, getState) => {
    axios.get("/admin/allergens/").then((response) =>
      dispatch({
        type: GET_ALLERGENS,
        payload: createAllergenManager(response.data.data),
      })
    );
  };
}

export function clearAllergens() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ALLERGENS });
  };
}
