import axios from "redux/actions/axios";
import {
  CREATE_PRODUCT,
  UPDATE_PRODUCT,
  CLEAR_PRODUCT,
  GET_PRODUCT,
} from "redux/actions/action-types";

import Product from "domain-objects/product";

export function createProduct(product) {
  return async (dispatch, getState) => {
    await axios
      .post("/admin/products/", product.renderJSON())
      .then((response) => {
        dispatch({ type: CREATE_PRODUCT, payload: new Product(product) });
      });
  };
}

export function updateProduct(product) {
  return async (dispatch, getState) => {
    await axios
      .put(`/admin/products/${product.id}/`, product.renderJSON())
      .then((response) => {
        dispatch({ type: UPDATE_PRODUCT, payload: new Product(product) });
      });
  };
}

export function clearProduct() {
  return (dispatch, getState) => dispatch({ type: CLEAR_PRODUCT });
}

export function getProduct(productId) {
  return (dispatch, getState) => {
    axios.get(`/admin/products/${productId}/`).then((response) => {
      dispatch({
        type: GET_PRODUCT,
        payload: new Product(response.data.data),
      });
    });
  };
}
