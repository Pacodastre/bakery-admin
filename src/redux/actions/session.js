import Cookies from "js-cookie";

import axios from "redux/actions/axios";
import { LOGIN } from "redux/actions/action-types";

export function login(session, callback) {
  return async (dispatch, getState) => {
    await axios
      .post("/login/", { session: session.renderJSON() })
      .then((response) => {
        dispatch({ type: LOGIN });
        axios.defaults.headers.common.Authorization = `Bearer ${response.data.jwt}`;
        Cookies.set("jwt", response.data.jwt);
      })
      .catch((error) => {
        alert("Wrong email or password");
      });
  };
}

export function logout() {
  return (dispatch, getState) => {
    Cookies.remove("jwt");
  };
}

export function isUserLoggedIn() {
  return !!Cookies.get("jwt");
}
