import axios from "redux/actions/axios";
import { GET_IMAGES, CLEAR_IMAGES } from "redux/actions/action-types";

import { createImageManager } from "domain-objects/image-manager";

export function getImages() {
  return (dispatch, getState) => {
    axios.get("/admin/images/").then((response) =>
      dispatch({
        type: GET_IMAGES,
        payload: createImageManager(response.data.data),
      })
    );
  };
}

export function clearImages() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_IMAGES });
  };
}
