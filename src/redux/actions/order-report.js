import axios from "redux/actions/axios";

export function downloadReport() {
  return (dispatch, getState) => {
    axios({
      url: `/admin/orders/report`,
      method: "GET",
      responseType: "blob",
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "report.xlsx");
      document.body.appendChild(link);
      link.click();
    });
  };
}
