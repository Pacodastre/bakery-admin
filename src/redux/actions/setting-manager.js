import axios from "redux/actions/axios";
import {
  GET_SETTINGS,
  CLEAR_SETTINGS,
  UPDATE_SETTINGS,
} from "redux/actions/action-types";

import { createSettingManager } from "domain-objects/setting-manager";

export function getSettings() {
  return (dispatch, getState) => {
    axios.get("/settings/").then((response) =>
      dispatch({
        type: GET_SETTINGS,
        payload: createSettingManager(response.data.data),
      })
    );
  };
}

export function clearSettings() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_SETTINGS });
  };
}

export function updateSettings(settingManager) {
  return async (dispatch, getState) => {
    await axios
      .put("/admin/settings/", settingManager.renderJSON())
      .then((response) =>
        dispatch({
          type: UPDATE_SETTINGS,
          payload: createSettingManager(response.data.data),
        })
      );
  };
}
