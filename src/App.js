import React from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Provider } from "react-redux";

import AppLayout from "layouts/app-layout";
import store from "redux/store";

import ProtectedRoute from "components/session/protected-route";

import ProductListPage from "pages/product-list";
import CreateProductPage from "pages/create-product";
import UpdateProductPage from "pages/update-product";
import OrderListPage from "pages/order-list";
import ApprovedOrderListPage from "pages/approved-order-list";
import ViewOrderPage from "pages/view-order";
import ViewOrderReceiptPage from "pages/view-order-receipt";
import RecipeListPage from "pages/recipe-list";
import UpdateRecipePage from "pages/update-recipe";
import MessageListPage from "pages/message-list";
import ImageListPage from "pages/image-list";
import AllergenListPage from "pages/allergen-list";
import CreateAllergenPage from "pages/create-allergen";
import EditAllergenPage from "pages/edit-allergen";
import CreateImagePage from "pages/create-image";
import LoginPage from "pages/login";
import BookingSlotListPage from "pages/booking-slot-list";
import EditOrderPage from "pages/edit-order";
import CreateOrderPage from "pages/create-order";
import SelectOrdersToPrintPage from "pages/select-orders-to-print";
import PrintReceiptsPage from "pages/print-receipts";
import SpecialOfferListPage from "pages/special-offer-list";
import CreateSpecialOfferPage from "pages/create-special-offer";
import UpdateSpecialOfferPage from "pages/update-special-offer";
import ReportsPage from "pages/reports";
import SettingListPage from "pages/setting-list";
import SettingsMenu from "components/settings/menu";
import EditAboutPage from "pages/edit-about";

import { formatDateToBackend } from "utils/date";

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <ProtectedRoute
          exact
          path="/orders/:id/receipt"
          component={ViewOrderReceiptPage}
        />
        <ProtectedRoute
          exact
          path="/orders/print-receipts/:orderId1Part1-:orderId1Part2-:orderId1Part3-:orderId1Part4-:orderId1Part5"
          component={PrintReceiptsPage}
        />
        <ProtectedRoute
          exact
          path="/orders/print-receipts/:orderId1Part1-:orderId1Part2-:orderId1Part3-:orderId1Part4-:orderId1Part5/:orderId2Part1-:orderId2Part2-:orderId2Part3-:orderId2Part4-:orderId2Part5"
          component={PrintReceiptsPage}
        />
        <ProtectedRoute
          exact
          path="/orders/print-receipts/:orderId1Part1-:orderId1Part2-:orderId1Part3-:orderId1Part4-:orderId1Part5/:orderId2Part1-:orderId2Part2-:orderId2Part3-:orderId2Part4-:orderId2Part5/:orderId3Part1-:orderId3Part2-:orderId3Part3-:orderId3Part4-:orderId3Part5"
          component={PrintReceiptsPage}
        />
        <ProtectedRoute
          exact
          path="/orders/print-receipts/:orderId1Part1-:orderId1Part2-:orderId1Part3-:orderId1Part4-:orderId1Part5/:orderId2Part1-:orderId2Part2-:orderId2Part3-:orderId2Part4-:orderId2Part5/:orderId3Part1-:orderId3Part2-:orderId3Part3-:orderId3Part4-:orderId3Part5/:orderId4Part1-:orderId4Part2-:orderId4Part3-:orderId4Part4-:orderId4Part5"
          component={PrintReceiptsPage}
        />
        <AppLayout>
          <Switch>
            <ProtectedRoute exact path="/" component={React.Fragment}>
              <Redirect to="/orders/confirmed" />
            </ProtectedRoute>
            <Route exact path="/login">
              <LoginPage />
            </Route>
            <ProtectedRoute exact path="/orders" component={React.Fragment}>
              <Redirect to="/orders/confirmed" />
            </ProtectedRoute>
            <ProtectedRoute
              exact
              path="/orders/waiting"
              component={OrderListPage}
              status="waiting"
            />
            <ProtectedRoute
              exact
              path="/orders/confirmed"
              component={OrderListPage}
              status="confirmed"
            />
            <ProtectedRoute
              path="/orders/approved"
              component={ApprovedOrderListPage}
              status="approved"
            />
            <ProtectedRoute
              exact
              path="/orders/declined"
              component={OrderListPage}
              status="declined"
            />
            <ProtectedRoute
              exact
              path="/orders/delivered"
              component={OrderListPage}
              status="delivered"
            />
            <ProtectedRoute
              exact
              path="/orders/new"
              component={CreateOrderPage}
            />
            <ProtectedRoute
              exact
              path="/orders/print-receipts"
              component={SelectOrdersToPrintPage}
            />
            <ProtectedRoute
              exact
              path="/orders/:id"
              component={ViewOrderPage}
            />
            <ProtectedRoute
              exact
              path="/orders/:id/edit"
              component={EditOrderPage}
            />
            <ProtectedRoute
              exact
              path="/products"
              component={ProductListPage}
            />
            <ProtectedRoute
              exact
              path="/products/new"
              component={CreateProductPage}
            />
            <ProtectedRoute
              exact
              path="/products/:id/edit"
              component={UpdateProductPage}
            />
            <ProtectedRoute exact path="/recipes" component={RecipeListPage} />
            <ProtectedRoute
              exact
              path="/messages"
              component={MessageListPage}
            />
            <ProtectedRoute exact path="/images" component={ImageListPage} />
            <ProtectedRoute
              exact
              path="/images/new"
              component={CreateImagePage}
            />

            <ProtectedRoute
              exact
              path="/recipes/:recipeId/edit"
              component={UpdateRecipePage}
            />
            <ProtectedRoute
              exact
              path="/booking-slots/:year-:month-:day"
              component={BookingSlotListPage}
            />
            <ProtectedRoute
              exact
              path="/booking-slots"
              component={React.Fragment}
            >
              <Redirect
                to={`/booking-slots/${formatDateToBackend(new Date())}`}
              />
            </ProtectedRoute>
            <ProtectedRoute
              exact
              path="/special-offers"
              component={SpecialOfferListPage}
            />
            <ProtectedRoute
              exact
              path="/special-offers/new"
              component={CreateSpecialOfferPage}
            />
            <ProtectedRoute
              exact
              path="/special-offers/:specialOfferID/edit"
              component={UpdateSpecialOfferPage}
            />
            <ProtectedRoute exact path="/reports" component={ReportsPage} />
            <SettingsMenu>
              <ProtectedRoute
                exact
                path="/settings/allergens"
                component={AllergenListPage}
              />
              <ProtectedRoute
                exact
                path="/settings/allergens/new"
                component={CreateAllergenPage}
              />
              <ProtectedRoute
                exact
                path="/settings/allergens/:id/edit"
                component={EditAllergenPage}
              />
              <ProtectedRoute
                exact
                path="/settings/configuration"
                component={SettingListPage}
              />
              <ProtectedRoute
                exact
                path="/settings/about"
                component={EditAboutPage}
              />
            </SettingsMenu>
          </Switch>
        </AppLayout>
      </Switch>
    </Router>
  </Provider>
);

export default App;
