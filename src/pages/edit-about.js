import React from "react";
import { Segment, Header } from "semantic-ui-react";

import EditAbout from "components/about/edit";

const EditAboutPage = () => {
  return (
    <Segment>
      <Header as="h1">Edit About</Header>
      <EditAbout />
    </Segment>
  );
};

export default EditAboutPage;
