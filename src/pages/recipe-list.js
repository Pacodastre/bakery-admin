import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";

import { getRecipes, clearRecipes } from "redux/actions/recipe-manager";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import RecipeList from "components/recipes/list";

const RecipeListPage = ({
  getRecipes,
  getProducts,
  clearProducts,
  clearRecipes,
  productManager,
  recipeManager,
}) => {
  useEffect(() => {
    getProducts();
    getRecipes();
    return () => {
      clearProducts();
      clearRecipes();
    };
  }, [getProducts, getRecipes, clearProducts, clearRecipes]);
  return (
    <Segment>
      <Header as="h1">Recipes</Header>
      <RecipeList />
    </Segment>
  );
};

const mapStateToProps = ({ orderManager }) => ({
  orderManager,
});

const mapDispatchToProps = {
  getRecipes,
  getProducts,
  clearProducts,
  clearRecipes,
};

export default connect(mapStateToProps, mapDispatchToProps)(RecipeListPage);
