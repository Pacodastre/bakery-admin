import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";

import { getOrders } from "redux/actions/order-manager";
import { getOrderStatuses } from "redux/actions/order-status-manager";
import SelectOrdersToPrint from "components/orders/select-orders-to-print";

const SelectOrdersToPrintPage = ({ getOrders, getOrderStatuses }) => {
  useEffect(() => {
    getOrders();
    getOrderStatuses();
  }, [getOrders, getOrderStatuses]);
  return (
    <Segment>
      <Header as="h1">Print Receipts</Header>
      <SelectOrdersToPrint />
    </Segment>
  );
};

const mapStateToProps = ({ imageManager }) => ({
  imageManager,
});

const mapDispatchToProps = {
  getOrders,
  getOrderStatuses,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectOrdersToPrintPage);
