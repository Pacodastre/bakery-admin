import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { useParams } from "react-router-dom";

import UpdateSpecialOffer from "components/special-offers/update";
import { getProducts } from "redux/actions/product-manager";
import { getSpecialOffer } from "redux/actions/special-offer";

const UpdateSpecialOfferPage = ({ getProducts, getSpecialOffer }) => {
  const { specialOfferID } = useParams();
  useEffect(() => {
    getProducts();
    getSpecialOffer(specialOfferID);
  }, [getProducts, getSpecialOffer, specialOfferID]);

  return (
    <Segment>
      <Header as="h1">Update Special Offer</Header>
      <UpdateSpecialOffer />
    </Segment>
  );
};

const mapDispatchToProps = { getProducts, getSpecialOffer };

export default connect(null, mapDispatchToProps)(UpdateSpecialOfferPage);
