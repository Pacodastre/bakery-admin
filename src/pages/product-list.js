import React from "react";

import { Link } from "react-router-dom";
import { Button, Segment, Header } from "semantic-ui-react";

import ProductList from "components/products/list";

const ProductListPage = () => (
  <Segment>
    <Header as="h1">List of products</Header>
    <Link to="products/new">
      <Button>New</Button>
    </Link>
    <ProductList />
  </Segment>
);

export default ProductListPage;
