import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Segment, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

import OrderCardList from "components/orders/list";
import { getOrders, clearOrders } from "redux/actions/order-manager";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import StatusTabBar from "components/orders/status-tab-bar";
import {
  getSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";

const OrderListPage = ({
  getOrders,
  getProducts,
  clearProducts,
  clearOrders,
  orderManager,
  status,
  getOrderStatuses,
  clearOrderStatuses,
  getSpecialOffers,
  clearSpecialOffers,
}) => {
  useEffect(() => {
    async function fetch() {
      await getOrderStatuses();
      await getProducts();
      await getSpecialOffers();
      await getOrders();
    }
    fetch();
    return () => {
      clearProducts();
      clearOrders();
      clearOrderStatuses();
      clearSpecialOffers();
    };
  }, [
    getProducts,
    getOrders,
    getOrderStatuses,
    getSpecialOffers,
    clearProducts,
    clearOrders,
    clearOrderStatuses,
    clearSpecialOffers,
  ]);
  return (
    <Segment>
      <Header as="h1">Orders</Header>
      <Link to="/orders/new">
        <Button>New order</Button>
      </Link>
      <StatusTabBar status={status} />
      <OrderCardList status={status} />
    </Segment>
  );
};

const mapStateToProps = ({ orderManager }) => ({
  orderManager,
});

const mapDispatchToProps = {
  getOrders,
  getProducts,
  clearProducts,
  clearOrders,
  getOrderStatuses,
  clearOrderStatuses,
  getSpecialOffers,
  clearSpecialOffers,
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderListPage);
