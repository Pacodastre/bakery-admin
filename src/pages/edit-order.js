import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import EditOrder from "components/orders/edit";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";

const ViewOrderPage = ({
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  order,
}) => {
  useEffect(() => {
    getProducts();
    getOrderStatuses();
    return () => {
      clearProducts();
      clearOrderStatuses();
    };
  }, [getProducts, clearProducts, getOrderStatuses, clearOrderStatuses]);

  return (
    <Segment>
      <Header as="h2">Edit order {order.id}</Header>
      <EditOrder />
    </Segment>
  );
};

const mapStateToProps = ({ order }) => ({ order });

const mapDispatchToProps = {
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewOrderPage);
