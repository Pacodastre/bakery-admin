import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { useParams } from "react-router-dom";

import ViewOrder from "components/orders/view";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import {
  getSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";
import { getOrder, clearOrder } from "redux/actions/order";

const ViewOrderPage = ({
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  getSpecialOffers,
  clearSpecialOffers,
  getOrder,
  clearOrder,
}) => {
  const { id } = useParams();
  useEffect(() => {
    async function fetch() {
      await getProducts();
      await getOrderStatuses();
      await getSpecialOffers();
      await getOrder(id);
    }
    fetch();
    return () => {
      clearProducts();
      clearOrderStatuses();
      clearSpecialOffers();
      clearOrder();
    };
  }, [
    getProducts,
    clearProducts,
    getOrderStatuses,
    clearOrderStatuses,
    getSpecialOffers,
    clearSpecialOffers,
    getOrder,
    id,
    clearOrder,
  ]);

  return (
    <Segment>
      <Header as="h2">View order</Header>
      <ViewOrder />
    </Segment>
  );
};

const mapDispatchToProps = {
  getProducts,
  clearProducts,
  getOrderStatuses,
  clearOrderStatuses,
  getSpecialOffers,
  clearSpecialOffers,
  getOrder,
  clearOrder,
};

export default connect(null, mapDispatchToProps)(ViewOrderPage);
