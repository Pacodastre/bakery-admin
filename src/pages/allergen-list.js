import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { getAllergens } from "redux/actions/allergen-manager";
import AllergenList from "components/allergens/list";

const AllergenListPage = ({ getAllergens, allergenManager }) => {
  useEffect(() => {
    getAllergens();
  }, [getAllergens]);
  return (
    <Segment>
      <Header as="h1">Allergens</Header>
      <Link to="/settings/allergens/new">New allergen</Link>
      <AllergenList />
    </Segment>
  );
};

const mapStateToProps = ({ allergenManager }) => ({
  allergenManager,
});

const mapDispatchToProps = {
  getAllergens,
};

export default connect(mapStateToProps, mapDispatchToProps)(AllergenListPage);
