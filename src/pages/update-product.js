import React, { useEffect } from "react";
import { connect } from "react-redux";

import { Link, useParams } from "react-router-dom";
import { Segment, Header } from "semantic-ui-react";

import UpdateProduct from "components/products/update";

import { updateProduct, getProduct, clearProduct } from "redux/actions/product";
import { clearAllergens, getAllergens } from "redux/actions/allergen-manager";

const UpdateProductPage = ({
  updateProduct,
  product,
  getProduct,
  getAllergens,
  clearAllergens,
  clearProduct,
}) => {
  const { id } = useParams();
  useEffect(() => {
    getAllergens();
    getProduct(id);
    return () => {
      clearAllergens();
      clearProduct();
    };
  }, [getAllergens, getProduct, id, clearAllergens, clearProduct]);
  return (
    <Segment>
      <Header as="h1">Update product</Header>
      <Link to="/products">Back</Link>
      <UpdateProduct />
    </Segment>
  );
};

const mapDispatchToProps = {
  updateProduct,
  getProduct,
  clearProduct,
  clearAllergens,
  getAllergens,
};

export default connect(null, mapDispatchToProps)(UpdateProductPage);
