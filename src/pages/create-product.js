import React, { useEffect } from "react";
import { connect } from "react-redux";

import { Link } from "react-router-dom";
import { Segment, Header } from "semantic-ui-react";

import { createProduct } from "redux/actions/product";
import CreateProduct from "components/products/create";

import { clearAllergens, getAllergens } from "redux/actions/allergen-manager";

const CreateProductPage = ({ createProduct, getAllergens, clearAllergens }) => {
  useEffect(() => {
    async function fetch() {
      await getAllergens();
    }
    fetch();
    return () => clearAllergens();
  });
  return (
    <Segment>
      <Header as="h1">Create product</Header>
      <Link to="/products">Back</Link>
      <CreateProduct />
    </Segment>
  );
};

const mapDispatchToProps = { createProduct, getAllergens, clearAllergens };

export default connect(null, mapDispatchToProps)(CreateProductPage);
