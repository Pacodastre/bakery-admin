import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import CreateOrder from "components/orders/create";
import { getProducts, clearProducts } from "redux/actions/product-manager";

const ViewOrderPage = ({ getProducts, clearProducts, order }) => {
  useEffect(() => {
    getProducts();
    return () => {
      clearProducts();
    };
  }, [getProducts, clearProducts]);

  return (
    <Segment>
      <Header as="h2">Create order</Header>
      <CreateOrder />
    </Segment>
  );
};

const mapStateToProps = ({ order }) => ({ order });

const mapDispatchToProps = {
  getProducts,
  clearProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewOrderPage);
