import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";
import { useParams } from "react-router-dom";

import EditAllergen from "components/allergens/edit";
import { getAllergen, clearAllergen } from "redux/actions/allergen";

const EditAllergenPage = ({ getAllergen, clearAllergen }) => {
  const { id } = useParams();
  useEffect(() => {
    getAllergen(id);
    // return () => clearAllergen();
  }, [id, getAllergen, clearAllergen]);

  return (
    <Segment>
      <Header as="h1">Edit Allergen</Header>
      <EditAllergen />
    </Segment>
  );
};

const mapDispatchToProps = { getAllergen, clearAllergen };

export default connect(null, mapDispatchToProps)(EditAllergenPage);
