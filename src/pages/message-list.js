import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";

import { getMessages } from "redux/actions/message-manager";
import MessageList from "components/messages/list";

const MessageListPage = ({ getMessages, messageManager }) => {
  useEffect(() => {
    getMessages();
  }, [getMessages]);
  return (
    <Segment>
      <Header as="h1">Messages</Header>
      <MessageList />
    </Segment>
  );
};

const mapStateToProps = ({ orderManager }) => ({
  orderManager,
});

const mapDispatchToProps = {
  getMessages,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageListPage);
