import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Button, Header, Menu, Segment, Table } from "semantic-ui-react";

import { MONTHS } from "utils/date";

import MonthlySalesChart from "components/reports/monthly-sales-chart";
import MonthlySalesByProductChart from "components/reports/monthly-sales-by-product-chart";
import { getOrders, clearOrders } from "redux/actions/order-manager";
import {
  getOrderStatuses,
  clearOrderStatuses,
} from "redux/actions/order-status-manager";
import { getProducts, clearProducts } from "redux/actions/product-manager";
import {
  getSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";

const Total = ({ orderManager }) => <>€{orderManager.calculateTotalSold()}</>;

const ProductTable = ({
  orderManager,
  specialOfferManager,
  productManager,
}) => (
  <Table>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Product</Table.HeaderCell>
        <Table.HeaderCell>Quantity</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {orderManager
        .groupByProduct(specialOfferManager)
        .map(({ product_id, quantity }) => (
          <Table.Row key={product_id}>
            <Table.Cell>
              {productManager.getProduct(product_id).name}
            </Table.Cell>
            <Table.Cell>{quantity}</Table.Cell>
          </Table.Row>
        ))}
    </Table.Body>
  </Table>
);

const CProductTable = connect(({ productManager, specialOfferManager }) => ({
  productManager,
  specialOfferManager,
}))(ProductTable);

const ReportsPage = ({
  getOrders,
  clearOrders,
  orderManager,
  orderStatusManager,
  getOrderStatuses,
  clearOrderStatuses,
  specialOfferManager,
  productManager,
  getProducts,
  clearProducts,
  getSpecialOffers,
  clearSpecialOffers,
}) => {
  useEffect(() => {
    async function fetch() {
      await getOrderStatuses();
      await getProducts();
      await getSpecialOffers();
      await getOrders();
    }
    fetch();

    return () => {
      clearOrderStatuses();
      clearOrders();
      clearProducts();
      clearSpecialOffers();
    };
  }, [
    getOrders,
    clearOrders,
    getOrderStatuses,
    clearOrderStatuses,
    getProducts,
    getSpecialOffers,
    clearProducts,
    clearSpecialOffers,
  ]);

  const [selectedMonth, setSelectedMonth] = useState(MONTHS[0]);

  return (
    <Segment>
      <Header as="h1">Reports</Header>
      Total: <Total orderManager={orderManager} />
      <MonthlySalesChart orderManager={orderManager} />
      <MonthlySalesByProductChart orderManager={orderManager} />
      <Segment>
        <Menu>
          {MONTHS.map((month) => (
            <Menu.Item
              key={month}
              as={Button}
              active={selectedMonth === month}
              onClick={() => setSelectedMonth(month)}
            >
              {month}
            </Menu.Item>
          ))}
        </Menu>
      </Segment>
      Monthly total:{" "}
      <Total orderManager={orderManager.filterMonth(selectedMonth)} />
      <br />
      All products sold:
      <CProductTable orderManager={orderManager.filterMonth(selectedMonth)} />
    </Segment>
  );
};

const mapStateToProps = ({
  specialOfferManager,
  orderManager,
  orderStatusManager,
  productManager,
}) => ({
  specialOfferManager,
  orderManager: orderManager.filterStatus(
    orderStatusManager.getByStatus("delivered").id
  ),
  orderStatusManager,
  productManager,
});

const mapDispatchToProps = {
  getOrders,
  clearOrders,
  getOrderStatuses,
  clearOrderStatuses,
  getProducts,
  clearProducts,
  getSpecialOffers,
  clearSpecialOffers,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportsPage);
