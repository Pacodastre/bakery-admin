import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Header, Segment } from "semantic-ui-react";

import { getSettings } from "redux/actions/setting-manager";
import SettingList from "components/settings/list";

const SettingListPage = ({ getSettings }) => {
  useEffect(() => {
    getSettings();
    return () => {};
  }, [getSettings]);
  return (
    <Segment>
      <Header as="h1">Settings</Header>
      <SettingList />
    </Segment>
  );
};

const mapDispatchToProps = {
  getSettings,
};

export default connect(null, mapDispatchToProps)(SettingListPage);
