import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Segment, Header } from "semantic-ui-react";

import CreateSpecialOffer from "components/special-offers/create";
import { getProducts } from "redux/actions/product-manager";

const CreateSpecialOfferPage = ({ getProducts }) => {
  useEffect(() => {
    getProducts();
  }, [getProducts]);

  return (
    <Segment>
      <Header as="h1">Create Special Offer</Header>
      <CreateSpecialOffer />
    </Segment>
  );
};

const mapDispatchToProps = { getProducts };

export default connect(null, mapDispatchToProps)(CreateSpecialOfferPage);
