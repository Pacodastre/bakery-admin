import moment from "moment";

export function isDateSame(date1, date2) {
  const date1WithoutTime = moment(
    moment(date1).format("DD/MM/YYYY"),
    "DD/MM/YYYY"
  );
  const date2WithoutTime = moment(
    moment(date2).format("DD/MM/YYYY"),
    "DD/MM/YYYY"
  );
  return date1WithoutTime.isSame(date2WithoutTime);
}

export function isMonthSame(date1, date2) {
  const date1WithoutTime = moment(
    moment(date1).format("DD/MM/YYYY"),
    "DD/MM/YYYY"
  );
  const date2WithoutTime = moment(
    moment(date2).format("DD/MM/YYYY"),
    "DD/MM/YYYY"
  );
  return date1WithoutTime.isSame(date2WithoutTime, "month");
}

export function formatDate(date) {
  return moment(date).format("DD/MM/YYYY");
}

export function formatDateTime(date) {
  return moment.utc(date).local().format("DD/MM/YYYY HH:mm");
}

export function formatDateToBackend(date) {
  return moment(date).format("YYYY-MM-DD");
}

export function formatDateTimeToBackend(datetime) {
  return moment(datetime).toISOString();
}

export const MONTHS = [
  "January",
  "Febrary",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
