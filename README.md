## Environment variables

| Environment variable   | Description      | Default value |
|------------------------|------------------|---------------|
| REACT_APP_BACKEND_HOST | Backend hostname | http://localhost:4000/api |
| PORT                   | Port that this project will be running on | 3000 (change to 3001 when running locally) |
